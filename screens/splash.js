import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button, 
  Alert,
  TextInput,
  StatusBar
} from 'react-native';
import {StackNavigator} from 'react-navigation';


export class Splash extends Component {
	constructor(props){
		super(props)
	}
  render() {
	  
	const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
		<StatusBar
			backgroundColor="#002868"
			barStyle="light-content"
		/>
        <Text style={styles.welcome}>
			Welcome
        </Text>
		
		<View style={styles.something}>
			<Button 
			onPress={() => {
				navigate('Courses_Page');
			}}
				color="#003fa5"
				title= "View Courses"
			/>
		</View>
		
		<View style={styles.something}>
			<Button
				onPress={() => {
				navigate('Schedule_Page');
			}}
				title= "View Schedule"
			/>
		</View>
		
		<View style={styles.something} >
			<Button color="#0062ff"
				title= "View Grades"
			/>
		</View>
		
      </View>
    );
  }
}

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  something:{
	  padding: 5,
	  width: 300,
	  color: "#003fa5",
  },
});