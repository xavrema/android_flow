import React, { Component } from 'react';
import {
	Platform,
	StyleSheet,
	Text,
	View,
	Button,
	Alert,
	TextInput,
	StatusBar,
	AsyncStorage,
	FlatList
} from 'react-native';
import { StackNavigator } from 'react-navigation';

class Assignment {
	constructor(cn, n, w, d) {
		this.course_name= cn;
		this.name = n;
		this.weight = w;
		this.date = d;
	}
}


export class Schedule extends Component {
	constructor(props) {
		super(props)
		this.state = { isLoading: true, dataSource: "", AssignmentColl: "" }
	}
	componentDidMount() {
		return fetch('http://remacle.dev.fast.sheridanc.on.ca/Courses.json')
			.then((response) => response.json())
			.then((responseJson) => {

				this.setState({
					isLoading: false,
					dataSource: responseJson.Courses,
				}, function () {

					this.load_page();
				});

			})
			.catch((error) => {
				Alert.alert(error + "");

			});

	}


	orderDate(first, second) {

		let date1 = first.split("/");
		let date2 = second.split("/");


		for (let i = 2; i >= 0; i -= 2) {
			if (parseInt(date1[i]) > parseInt(date2[i]))
				return 1;
			else if (parseInt(date1[i]) < parseInt(date2[i]))
				return 0;

			if (i == 0)
				i = 3;
		}
		return 0;



	}

	sortDates(items) {

		let stop = false;

		while (!stop) {
			stop = true;
			for (let i = 0; i < items.length - 1; i++) {

				if (this.orderDate(items[i].date, items[i + 1].date) == 1) {

					let temp = items[i];
					items[i] = items[i + 1]
					items[i + 1] = temp;
					stop = false;

				}
			}
		}
		return items;

	}

	async load_page() {
		var savedAssignments = [];
		for (let i = 0; i < 5; i++) {
			let course = this.state.dataSource[i]
			for (let j = 0; j < 5; j++) {
				let assignment = course.Assignments[j];
				var weight = assignment.weight;
				var due_date = assignment.DueDate;
				var obj = new Assignment(
					course.Name,
					('A ' + j),
					weight,
					due_date
				);

				savedAssignments.push(obj);
			}
		}
		this.setState({ AssignmentColl: this.sortDates(savedAssignments) });
	}



	render() {
		const { navigate } = this.props.navigation;
		return (
			<View style={styles.container}>
				<StatusBar
					backgroundColor="#002868"
					barStyle="light-content"
				/>
				 <FlatList
          data={this.state.AssignmentColl}
          renderItem={({item}) => <Text style={styles.something}>{item.course_name} --> {item.name} Due: {item.date}</Text>}
        />
			</View>
		);
	}
}

export default Schedule;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	something: {
		padding: 5,
		width: 300,
		color: "#003fa5",
	},
});