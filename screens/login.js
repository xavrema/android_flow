import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button, 
  Alert,
  TextInput,
  AsyncStorage,
  NetInfo
} from 'react-native';
import {StackNavigator} from 'react-navigation';

const instructions = Platform.select({
  ios: 'You are using iOS',
  android: 'You are using Android',
});



export class Login extends Component {
	constructor(props){
		super(props)
		this.state = {msg: "", isLoading:true, input_username:"", input_password:""}
		NetInfo.isConnected.fetch().then(isConnected => {
		(isConnected ? function(){
			this.setState({msg:"Login"});
		  } : function(){
			  this.setState({msg:"Please Connect to Network"});
			  Alert.alert("Please Connect to the Network and Restart the App");
			  
		  })
		});
		//for usage: {this.state.msg}
		//for second usage {this.state.another_msg}
	}
	componentDidMount(){
		return fetch('http://remacle.dev.fast.sheridanc.on.ca/users.json')
		.then((response) => response.json())
		.then((responseJson) => {
			
			this.setState({
				isLoading: false,
				dataSource: responseJson.Users,
			}, function(){
				
			});
			
			}) 
		.catch((error) =>{
			Alert.alert(error+"");
			
		});
		
	}
	
	onUsernameType(text) {
		this.state.input_username = text;
	}
	
	onPasswordType(text) {
		this.state.input_password = text;
	}
	async saveKey(user,index) {
        try {
			for(let i=0;i<5;i++){
				await AsyncStorage.setItem("@CurrentUser:key",user);
				await AsyncStorage.setItem('@'+user+'Course'+i+':key', this.state.dataSource[index].Courses[i]);//<----Modify Key
			}
            
			
        } catch (error) {
            console.log("Error saving data" + error);
        }
    }
	
	_validate_login(){
		var iUser;
		for (iUser = 0; iUser < 5; iUser++) {
			if (this.state.input_username == this.state.dataSource[iUser].Username) {
				if (this.state.input_password == this.state.dataSource[iUser].Password) {
					this.saveKey(this.state.dataSource[iUser].Username,iUser);
					return true;
				}
			}
			
		}
		this.setState({input_username: "", input_password: ""});
		return false;
	}
	
  render() {
	const {navigate} = this.props.navigation;
	
	return (
	
      <View style={styles.container}>
        <Text style={styles.welcome}>
			{this.state.msg}
        </Text>
		
		<TextInput 
			onChangeText={(text) => this.onUsernameType(text)}
			style={styles.something} placeholder="Username"/>
		<TextInput 
			onChangeText={(text) => this.onPasswordType(text)}
			style={styles.something} type="password"
			placeholder="Password"/>
		
        <Text style={styles.instructions}>
          {instructions}
        </Text>
		
		<Button
			onPress={() => {
				if (this._validate_login() == true) {
					navigate('Splash_Page');
				} else {
					Alert.alert("Invalid Credentials");
				}
			}}
			title= "Login"
			/>
      </View>
    );
  }
}

export default Login;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  something:{
	  width: 200,
  },
});
