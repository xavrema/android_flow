import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button, 
  Alert,
  TextInput,
  StatusBar,
  AsyncStorage
} from 'react-native';
import {StackNavigator} from 'react-navigation';

const instructions = Platform.select({
  ios: 'You are using iOS',
  android: 'You are using Android',
});

export class Course extends Component {
		constructor(props){
			super(props)
			this.state ={
				title:"Press Me",
				Course1:"",
				Course2:"",
				Course3:"",
				Course4:"",
				Course5:""
			}
		}
		
		
		
		async load_Name(){
			try {
				
				const user = await AsyncStorage.getItem("@CurrentUser:key");//<----Modify Key
				
				//await AsyncStorage.setItem('@'+user+'Course'+i+':key', this.state.dataSource[index].Courses[i]);
				let temp1 = await AsyncStorage.getItem('@'+user+'Course0:key');
				let temp2 = await AsyncStorage.getItem('@'+user+'Course1:key');
				let temp3 = await AsyncStorage.getItem('@'+user+'Course2:key');
				let temp4 = await AsyncStorage.getItem('@'+user+'Course3:key');
				let temp5 = await AsyncStorage.getItem('@'+user+'Course4:key');
				this.setState({Course1:temp1});
				this.setState({Course2:temp2});
				this.setState({Course3:temp3});
				this.setState({Course4:temp4});
				this.setState({Course5:temp5});

				
			} catch (error) {
				this.setState({Course1:error+""});
				//Alert.alert("error"
			}
		}
		
		
		
  render() {
	this.load_Name();
    return (
      <View style={styles.container}>
		<StatusBar
			backgroundColor="#002868"
			barStyle="light-content"
		/>
		<View style={styles.something}>
			<Button 
			  title={this.state.Course1}
			/>
		</View>
		<View style={styles.something}>
			<Button
			  title={this.state.Course2}
			/>
		</View>
		<View style={styles.something}>
			<Button
			  title={this.state.Course3}
			/>
		</View>
		<View style={styles.something}>
			<Button
			  title={this.state.Course4}
			/>
		</View>
		<View style={styles.something}>
			<Button
			  title={this.state.Course5}
			/>
		</View>

		
      </View>
    );
  }
}

export default Course;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  something:{
	  padding: 5,
	  width: 300,
	  color: "#003fa5",
  },
});