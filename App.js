import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button, 
  Alert,
  TextInput,
  AppRegistry,
  NetInfo
} from 'react-native';
import {StackNavigator} from 'react-navigation';

import Login from './screens/login';
import Splash from './screens/splash';
import Course from './screens/courses';
import Schedule from './screens/schedule';

export default class App extends Component{
	render(){
		return<RootStack/>;
		
	}
}

const RootStack = StackNavigator(
	{
		
		Login_Page:{
			screen: Login,
		},
		Splash_Page: {
			screen: Splash,
		},
		Courses_Page:{
			screen: Course,
		},Schedule_Page:{
			screen: Schedule,
		},
		
	},
	{
		initialRouteName: 'Login_Page',
	}
);

AppRegistry.registerComponent('AwesomeProject',()=>App)